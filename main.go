package main

import (
	"os"

	"github.com/davecgh/go-spew/spew"
	"github.com/gomodule/redigo/redis"
)

func main() {
	spew.Dump(os.Getenv("REDIS_PW"))
	conn, err := redis.Dial("tcp", "redis-10496.c10.us-east-1-4.ec2.cloud.redislabs.com:10496", redis.DialPassword(os.Getenv("REDIS_PW")))
	if err != nil {
		spew.Dump(err)
	}

	spew.Dump(conn.Do("KEYS", "*"))
}
