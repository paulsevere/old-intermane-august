package githook

import (
	"encoding/json"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	"gitlab.com/paulsevere/intermane/pkg/cache"
)

func Register(router *mux.Router) {
	hookrouter := router.PathPrefix("/githook").Subrouter()
	hookrouter.Methods("GET").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rc := cache.Conn()
		rc.Do("SET", "FOO", "BAR")
		w.Write([]byte("THIS IS THE GITHOOK"))
	})

	hookrouter.Methods("POST").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var postBody map[string]interface{}
		err := json.NewDecoder(r.Body).Decode(&postBody)
		if err != nil {
			http.Error(w, "Unable to parse body", 400)
		}
		spew.Dump(postBody)

	})

}
