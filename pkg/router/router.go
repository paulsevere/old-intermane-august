package router

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/paulsevere/intermane/pkg/cache"
	"gitlab.com/paulsevere/intermane/pkg/githook"
)

func Serve() {
	cache.Init()
	router := mux.NewRouter()
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("HELLO WORLD"))
	})
	githook.Register(router)
	log.Fatal(http.ListenAndServe(":3000", router))

}
