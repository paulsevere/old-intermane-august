package bitbucket

import (
	"fmt"

	"gitlab.com/paulsevere/intermane/pkg/config"
)

type Stub struct {
	Branch string
	Method string
	Path   string
}

func (s *Stub) IntoRequestURL() string {
	reqURL := fmt.Sprintf(`%s/raw/stubs/%s%s.json?at=%s`, config.RepoURL(), s.Method, s.Path, s.Branch)
	println(reqURL)
	return reqURL
}
