package bitbucket

type Suite struct {
	Branch string
	Stubs  []*Stub
}
