package bitbucket

import (
	"net/http"

	"github.com/BurntSushi/toml"
)

type Config struct {
	Proxy map[string]string
}

var REPO_URL = "https://git.forge.lmig.com/projects/USCMIMMA/repos/intermane-suites"
var RAW_SRC_URL = REPO_URL + "/raw"

func FetchConfig(suiteName string) (config *Config, err error) {
	resp, err := http.Get(RAW_SRC_URL + "/config.toml?at=" + suiteName)
	if err != nil {
		return
	}
	var conf Config
	_, err = toml.DecodeReader(resp.Body, &conf)

	if err != nil {
		return

	}
	config = &conf

	return
}
