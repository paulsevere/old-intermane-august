package reverseproxy

import (
	"net/http"
	"strings"
)

var authCookieName = "INTERMAN_BASIC_AUTH"

func extractBasicAuthCredentials(req *http.Request) (username string, password string) {
	authCookie, err := req.Cookie(authCookieName)
	if err != nil {
		return
	}
	segs := strings.Split(authCookie.Value, ":")
	username = segs[0]
	if len(segs) > 1 {
		password = segs[1]
	} else {
		password = "testing1"
	}
	return
}

func addUserToContext(req *http.Request) {

}

func getProxyHost(req *http.Request) (proxyHost string, err error) {

	return
}
