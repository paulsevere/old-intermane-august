package reverseproxy

import (
	"net/http"
	"net/http/httputil"
)

func director(req *http.Request) {
	req.URL.Host = "www.reqres.in"
	req.URL.Scheme = "https"
	req.URL.Path = "/api" + req.URL.Path
	req.Host = "www.reqres.in"
}

func CreateReverseProxy() *httputil.ReverseProxy {
	return &httputil.ReverseProxy{Director: director}
}
