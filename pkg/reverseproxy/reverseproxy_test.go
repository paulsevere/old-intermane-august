package reverseproxy

import (
	"net/http"
	"testing"
)

type example struct {
	username string
	password string
	cookie   string
}

func TestExtractBasicAuthCredentials(t *testing.T) {
	runs := []example{
		example{username: "paul", password: "rad", cookie: "paul:rad"},
		example{username: "paul", password: "rad", cookie: "paul:rad"},
	}
	for _, run := range runs {
		req, _ := http.NewRequest("GET", "/", nil)
		req.AddCookie(&http.Cookie{Value: run.cookie, Name: authCookieName})
		username, password := extractBasicAuthCredentials(req)
		if username != run.username && password != run.password {
			t.Fail()
		}
	}

}
