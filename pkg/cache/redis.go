package cache

import (
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/gomodule/redigo/redis"
)

var (
	pool *redis.Pool
	// redisServer = flag.String("redisServer", ":6379", "")
)

func newPool(addr string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr, redis.DialPassword("paulsevere"))
		},
	}
}

func Init() {

	if pool == nil {
		pool = newPool("redis-10496.c10.us-east-1-4.ec2.cloud.redislabs.com:10496")
		spew.Dump(pool)
	}
}

func Conn() redis.Conn {
	return pool.Get()
}
