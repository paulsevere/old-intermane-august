package config

import "os"

var repoURL = "https://git.forge.lmig.com/projects/USCMIMMA/repos/intermane-suites"

func RepoURL() string {
	if repoURL == "" {
		repoURL = os.Getenv("REPO_URL")
	}
	return repoURL
}
