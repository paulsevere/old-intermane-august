package ltpatoken

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/davecgh/go-spew/spew"
)

func createClient() *http.Client {
	return &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
}
func createRequest(username string, password string, env string) *http.Request {
	form := url.Values{}
	form.Add("USER", username)
	form.Add("PASSWORD", password)
	form.Add("target", createRedirectTarget(env))
	req, err := http.NewRequest("POST", createFccUrl(env), strings.NewReader(form.Encode()))
	if err != nil {
		println(err.Error())
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}

func createFccUrl(env string) string {
	eserviceHost := mapEnvironmentToHost(env)
	return eserviceHost + "/LMAuth/eservicelog.fcc"

}

func createRedirectTarget(env string) string {
	eserviceHost := mapEnvironmentToHost(env)
	return eserviceHost + "/PmEServiceAuth/cAuth/tredir.html?uri=home"
}

func FetchToken(username string, password string, env string) (token string, err error) {
	hc := createClient()
	res, err := hc.Do(createRequest(username, password, env))
	if err != nil {
		println(err.Error())
		return
	}
	token, err = findToken(res.Cookies())
	return

}

func findToken(cookies []*http.Cookie) (token string, err error) {
	for _, cookie := range cookies {
		if cookie.Name == "LtpaToken2" {
			token = cookie.Value
			spew.Dump(cookie.Expires)
			return
		}
	}
	return
}
