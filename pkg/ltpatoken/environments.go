package ltpatoken

import (
	"strings"
)

func subEnv(env string) string {
	return "https://" + env + ".service.libertymutual.com"
}

func mapEnvironmentToHost(name string) string {

	switch name {
	case "ete":
		return "https://ete-eservice.libertymutual.com"
	case "prod":
		return "https://eservice.libertymutual.com"
	}

	if strings.Contains(name, "-") {
		return subEnv(name)
	}
	return ""

}
