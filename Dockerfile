FROM golang
RUN go get gitlab.com/paulsevere/intermane
RUN echo $GOPATH
RUN which intermane
EXPOSE 3000
CMD ["intermane"]